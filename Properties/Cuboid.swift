//
//  Cuboid.swift
//  Properties
//
//  Created by R. Kukuh on 29/04/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

struct Cuboid {
    var width = 0.0, height = 0.0, depth = 0.0
    
    var volume: Double {
        return width * height * depth
    }
}
