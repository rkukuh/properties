//
//  Size.swift
//  Properties
//
//  Created by R. Kukuh on 29/04/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

struct Size {
    var width = 0.0, height = 0.0
}
