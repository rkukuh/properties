//
//  DataManager.swift
//  Properties
//
//  Created by R. Kukuh on 29/04/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

class DataManager {
    var data = [String]()
    
    lazy var importer = DataImporter()
}
