//
//  Point.swift
//  Properties
//
//  Created by R. Kukuh on 29/04/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

struct Point {
    var x = 0.0, y = 0.0
}
